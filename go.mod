module gitlab.com/cyclops-community/cdr-client-interface.git

go 1.16

require (
	github.com/go-openapi/errors v0.20.1
	github.com/go-openapi/runtime v0.21.0
	github.com/go-openapi/strfmt v0.21.0
	github.com/go-openapi/swag v0.19.15
	github.com/go-openapi/validate v0.20.3
	gitlab.com/cyclops-utilities/datamodels v0.0.0-20191016132854-e9313e683e5b
)
